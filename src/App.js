import React, {Component} from 'react';
import './App.css';
import Login from './views/Login';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false,
    }
  }

  componentDidMount() {

    // fetch()

  }

  render() {
    return (
      <div className="App">
        <Login/>
      </div>
    );
  }

}

export default App;
