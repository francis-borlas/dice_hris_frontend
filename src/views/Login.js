import React, { Component } from 'react';
// import { apiModule } from '../commons/apiCalls';
// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap/dist/css/bootstrap-theme.css';
// import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";

class Login extends Component {

	constructor(props) {
		super(props);

		this.state = {
			email: "",
			password: ""
		}
	}

	handleChange = event => {
        this.setState({
          [event.target.name]: event.target.value
		});
	}
	
	// async handleSubmit(event) {
	// 	event.preventDefault(); 
	// 	const that = this;
	// 	let bool = this.validateField(event);  
		
	// 	if (bool) {
	// 	  // let post = jsonToURI(postData);
	// 	  this.setState({isLoading : true});
	// 	  let postData = {email : this.state.email, password : this.state.password};
	// 	  let data = await apiModule('authentication_login', {}, postData);
		  
	// 	  if(data.result == 'success'){
	
	// 		this.setState({isLoading : false});
	// 		this.getSession(data);
		  
	// 	  } else {
	
	// 		this.setState({isLoading : false});
			
	// 		if(data.result == 'expired_password'){
	// 		 that.props.changePageHandler("ResetPassword"); 
	// 		} else {
	// 		//    toast.error(data.message, {
	// 		// 	  position: "bottom-right",
	// 		// 	  autoClose: 10000,
	// 		// 	  hideProgressBar: true,
	// 		// 	  closeOnClick: false,
	// 		// 	  pauseOnHover: true,
	// 		// 	  draggable: false
	// 		// 	});
	// 		}
	// 	  }
	// 	}
	// }

	render() {
		return(
			<div className="login-page-container">
				<div className="login-form-container center">
					<div className="login-form-inner">
						<div className="login-msg">
							Welcome,
							<div className="sub-login-msg">
								Sign In to continue
							</div>
						</div>
						<div className="login-form">
							<div className="form">
								<div className="form-group">
									<input className="login-inp" type="text" name="email" value={this.state.email} onChange={this.handleChange} placeholder="Username or Email Address"></input>
								</div>
								<div className="form-group">
									<input className="login-inp" type="password" name="password" value={this.state.password} onChange={this.handleChange} placeholder="Password"></input>
								</div>
								<div className="form-group remember-me-grp">
									<div className="">
										<label>
											<input type="checkbox" name="remember_me"></input>
											Remember Me
										</label>
									</div>
									<div className="">
										<a href="./">Forgot Password</a>
									</div>
								</div>
								<div className="btn">
									<input className="login-google-btn" type="button" value="Sign in with Google" />
								</div>
								<div className="btn">
									<input className="login-btn" type="submit" value="Submit" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Login;