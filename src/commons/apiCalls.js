import { formatDateByNumber, jsonToURI } from "./utility";
import { writeData, readAllData } from "./db";
import { myConfig } from "./config";
import { endpoints } from "./restEndpoints";

const axios = require("axios");

const createQueryStrings = obj => {
    const str = [];
    let returnString = "";
    for (const p in obj) {
        if (obj.hasOwnProperty(p)) {
            str.push(`${encodeURIComponent(p)}=${encodeURIComponent(obj[p])}`);
        }
    }

    if (str.length > 0) {
        returnString = `?${str.join("&")}`;
    }
    return returnString;
};

const apiModule = async (
    my_module,
    requestParams,
    formData,
    queryStrings,
    special
) => {
    const { url } = endpoints(requestParams)[my_module];

    const { headers } = endpoints(requestParams)[my_module];
    const { method } = endpoints(requestParams)[my_module];
    let server_url = myConfig.SERVER_URL;
    if (
        endpoints(requestParams)[my_module].server_url !== null &&
        endpoints(requestParams)[my_module].server_url !== undefined
    ) {
        server_url = endpoints(requestParams)[my_module].server_url;
    }
    const result = {};

    writeData("actions", { last_action: Date.now(), id: 1 });

    if (queryStrings === null || queryStrings === undefined) {
        queryStrings = "";
    }

    // Use axios whenver we want to use application/json as the body header. Otherwise we will use x-www-form-urlencoded
    if (special) {
        return new Promise((resolve, reject) => {
            const myUrl = server_url + url + queryStrings;
            axios({
                method,
                url: myUrl,
                data: formData,
                headers
            })
                .then(data => {
                    writeData("actions", { last_action: Date.now(), id: 1 });
                    resolve(data);
                })
                .catch(err => {
                    console.log(err);
                    // window.confirm(err);
                    writeData("actions", { last_action: Date.now(), id: 1 });
                    reject(err);
                });
        });
    }
    const data = jsonToURI(formData);
    return new Promise((resolve, reject) => {
        if (data === null || data === "") {
            fetch(server_url + url + queryStrings, {
                method,
                headers
            })
                .then(response => response.json())
                .then(data => {
                    writeData("actions", {
                        last_action: Date.now(),
                        id: 1
                    });
                    resolve(data);
                })
                .catch(err => {
                    writeData("actions", {
                        last_action: Date.now(),
                        id: 1
                    });
                    reject(err);
                });
        } else {
            fetch(server_url + url + queryStrings, {
                method,
                headers,
                body: data
            })
                .then(response => response.json())
                .then(data => {
                    writeData("actions", {
                        last_action: Date.now(),
                        id: 1
                    });
                    resolve(data);
                })
                .catch(err => {
                    writeData("actions", {
                        last_action: Date.now(),
                        id: 1
                    });
                    reject(err);
                });
        }
    });
};

const apiCall = (url, method, headers, formData) =>
    new Promise((resolve, reject) => {
        fetch(myConfig.SERVER_URL + url, {
            method,
            headers,
            body: formData
        })
            .then(response =>
                // console.log(response);
                response.json()
            )
            .then(data => {
                // console.log(data)
                resolve(data);
            })
            .catch(err => {
                console.log(err);
                // window.confirm(err);
                reject(err);
            });
    });

const offlineSync = (my_module, requestParams, formData) => {
    const { url } = endpoints(requestParams)[my_module];
    const { headers } = endpoints(requestParams)[my_module];
    const { method } = endpoints(requestParams)[my_module];
    const post = {};
    post.requestParams = { url, method, headers };
    post.data = { formData };
    let id = 0;
    return new Promise((resolve, reject) => {
        navigator.serviceWorker.ready.then(sw => {
            readAllData("sync-posts")
                .then(sync => {
                    id = sync.length + 1;
                    return id;
                })
                .then(id => {
                    writeData("sync-posts", { post, id }).then(() =>
                        sw.sync.register("sync-new-posts")
                    );

                    resolve({
                        message: "Post is saved for syncing",
                        result: "success"
                    });
                    return {
                        message: "Post is saved for syncing",
                        result: "success"
                    };
                });
        });
    });
};

export { apiCall, offlineSync, apiModule, createQueryStrings };