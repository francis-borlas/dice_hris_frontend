export const endpoints = (requestParams = {}) => ({

    /* AUTHENTICATION ENDPOINTS */
    authentication_login: {
        description: "Login user to the system",
        url: "/users/login",
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        sync: false
    }

});
